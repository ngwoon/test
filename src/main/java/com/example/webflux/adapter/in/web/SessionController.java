package com.example.webflux.adapter.in.web;

import com.example.webflux.application.port.in.CreateSessionUseCase;
import com.example.webflux.application.port.in.DeleteSessionUseCase;
import com.example.webflux.application.port.in.GetSessionUseCase;
import com.example.webflux.application.port.in.UpdateSessionUseCase;
import com.example.webflux.domain.Session;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.RequiredArgsConstructor;
import lombok.ToString;
import org.springframework.http.MediaType;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import reactor.core.publisher.Mono;

import javax.validation.Valid;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.PositiveOrZero;

@Validated
@RequiredArgsConstructor
@RestController
public class SessionController {

    private final CreateSessionUseCase createSessionUseCase;
    private final GetSessionUseCase getSessionUseCase;
    private final UpdateSessionUseCase updateSessionUseCase;
    private final DeleteSessionUseCase deleteSessionUseCase;

    @PostMapping(
            value = "/user",
            consumes = MediaType.APPLICATION_JSON_VALUE,
            produces = MediaType.APPLICATION_JSON_VALUE
    )
    public Mono<Session> createSession(@RequestBody UserInfo userInfo) {
        return createSessionUseCase.createSession(
                new CreateSessionUseCase.Command(
                        userInfo.getUserName(), userInfo.getUserEmail(), userInfo.getUserId()
                )
        );
    }

    @GetMapping(value = "/user/{userId}", produces = MediaType.APPLICATION_JSON_VALUE)
    public Mono<Session> getSession(@PathVariable("userId") Long userId) {
        return getSessionUseCase.getSession(new GetSessionUseCase.Command(userId));
    }

    @PutMapping(
            value = "/user/{userId}",
            produces = MediaType.APPLICATION_JSON_VALUE
    )
    public Mono<Session> updateSession(@PathVariable("userId") Long userId) {
        return updateSessionUseCase.updateSession(new UpdateSessionUseCase.Command(userId));
    }

    @DeleteMapping(value = "/user/{userId}")
    public Mono<Void> deleteSession(@PathVariable("userId") Long userId) {
        return deleteSessionUseCase.deleteSession(new DeleteSessionUseCase.Command(userId));
    }

    @ToString
    @Getter
    @NoArgsConstructor
    private static class UserInfo {

        @NotBlank
        @JsonProperty("user_name")
        private String userName;

        @Email
        @JsonProperty("user_email")
        private String userEmail;

        @PositiveOrZero
        @JsonProperty("user_id")
        private Long userId;
    }

    @ToString
    @Getter
    @NoArgsConstructor
    private static class UserInfoWithoutId {

        @NotBlank
        @JsonProperty("user_name")
        private String userName;

        @Email
        @JsonProperty("user_email")
        private String userEmail;
    }

    @ToString
    @Getter
    @NoArgsConstructor
    private static class SessionResponse {
        @JsonProperty("session_key")
        private String sessionKey;
    }
}
