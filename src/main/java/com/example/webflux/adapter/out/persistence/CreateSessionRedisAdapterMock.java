package com.example.webflux.adapter.out.persistence;

import com.example.webflux.application.port.out.CreateSessionPort;
import com.example.webflux.domain.Session;
import org.springframework.stereotype.Component;
import reactor.core.publisher.Mono;

import java.time.LocalDateTime;
import java.util.Random;

@Component
public class CreateSessionRedisAdapterMock implements CreateSessionPort {
    
    @Override
    public Mono<Session> createSession(String userName, String userEmail, Long userId) {
        return Mono.just(new Session() {
            @Override
            public String getUsername() {
                return "남관우";
            }

            @Override
            public String getUserEmail() {
                return "test-email@gmail.com";
            }

            @Override
            public Long getUserId() {
                Random rand = new Random();
                return rand.nextLong();
            }

            @Override
            public LocalDateTime getExpirationTime() {
                return LocalDateTime.now().plusMinutes(1);
            }
        });
    }
}
