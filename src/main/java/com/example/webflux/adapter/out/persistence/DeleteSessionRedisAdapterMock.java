package com.example.webflux.adapter.out.persistence;

import com.example.webflux.application.port.out.DeleteSessionPort;
import org.springframework.stereotype.Component;
import reactor.core.publisher.Mono;

@Component
public class DeleteSessionRedisAdapterMock implements DeleteSessionPort {
    
    @Override
    public Mono<Void> deleteSession(Long userId) {
        return Mono.empty().then();
    }
}
