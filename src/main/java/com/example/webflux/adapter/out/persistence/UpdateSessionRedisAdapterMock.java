package com.example.webflux.adapter.out.persistence;

import com.example.webflux.application.port.out.UpdateSessionPort;
import com.example.webflux.domain.Session;
import com.fasterxml.jackson.annotation.JsonInclude;
import org.springframework.stereotype.Component;
import reactor.core.publisher.Mono;

import java.time.LocalDateTime;

@JsonInclude

@Component
public class UpdateSessionRedisAdapterMock implements UpdateSessionPort {
    
    @Override
    public Mono<Session> updateSession(Long userId) {
        return Mono.just(new Session() {
            @Override
            public String getUsername() {
                return "남관우";
            }

            @Override
            public String getUserEmail() {
                return "test-email@gmail.com";
            }

            @Override
            public Long getUserId() {
                return userId;
            }

            @Override
            public LocalDateTime getExpirationTime() {
                return LocalDateTime.now().plusMinutes(1);
            }
        });
    }
}
