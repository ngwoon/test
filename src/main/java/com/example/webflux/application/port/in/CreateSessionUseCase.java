package com.example.webflux.application.port.in;

import com.example.webflux.domain.Session;
import lombok.Getter;
import org.springframework.validation.annotation.Validated;
import reactor.core.publisher.Mono;

import javax.validation.Valid;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

@Validated
public interface CreateSessionUseCase {
    Mono<Session> createSession(@Valid Command command);

    @Getter
    class Command {
        @NotBlank
        private final String userName;

        @NotBlank
        @Email
        private final String userEmail;

        @NotNull
        private final Long userId;

        public Command(String userName, String userEmail, Long userId) {
            this.userName = userName;
            this.userEmail = userEmail;
            this.userId = userId;
        }
    }
}
