package com.example.webflux.application.port.in;

import lombok.Getter;
import org.springframework.validation.annotation.Validated;
import reactor.core.publisher.Mono;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

@Validated
public interface DeleteSessionUseCase {
    Mono<Void> deleteSession(@Valid Command command);

    @Getter
    class Command {
        @NotNull
        private final Long userId;

        public Command(Long userId) {
            this.userId = userId;
        }
    }
}
