package com.example.webflux.application.port.in;

import com.example.webflux.domain.Session;
import lombok.Getter;
import org.springframework.validation.annotation.Validated;
import reactor.core.publisher.Mono;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

@Validated
public interface UpdateSessionUseCase {
    Mono<Session> updateSession(@Valid Command command);

    @Getter
    class Command {
        @NotNull
        private final Long userId;

        public Command(Long userId) {
            this.userId = userId;
        }
    }
}
