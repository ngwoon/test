package com.example.webflux.application.port.out;

import com.example.webflux.domain.Session;
import reactor.core.publisher.Mono;

public interface CreateSessionPort {
    Mono<Session> createSession(String userName, String userEmail, Long userId);
}
