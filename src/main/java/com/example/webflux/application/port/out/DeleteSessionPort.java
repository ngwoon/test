package com.example.webflux.application.port.out;

import reactor.core.publisher.Mono;

public interface DeleteSessionPort {
    Mono<Void> deleteSession(Long userId);
}
