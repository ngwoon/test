package com.example.webflux.application.port.out;

import com.example.webflux.domain.Session;
import reactor.core.publisher.Mono;

public interface GetSessionPort {
    Mono<Session> getSession(Long userId);
}
