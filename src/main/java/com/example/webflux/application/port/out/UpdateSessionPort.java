package com.example.webflux.application.port.out;

import com.example.webflux.domain.Session;
import reactor.core.publisher.Mono;

public interface UpdateSessionPort {
    Mono<Session> updateSession(Long userId);
}
