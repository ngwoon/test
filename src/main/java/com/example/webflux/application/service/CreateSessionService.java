package com.example.webflux.application.service;

import com.example.webflux.application.port.in.CreateSessionUseCase;
import com.example.webflux.application.port.out.CreateSessionPort;
import com.example.webflux.domain.Session;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Mono;

@RequiredArgsConstructor
@Service
public class CreateSessionService implements CreateSessionUseCase {

    private final CreateSessionPort createSessionPort;

    @Override
    public Mono<Session> createSession(Command command) {
        return createSessionPort.createSession(
                command.getUserName(), command.getUserEmail(), command.getUserId());
    }
}
