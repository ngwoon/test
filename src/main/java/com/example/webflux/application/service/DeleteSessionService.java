package com.example.webflux.application.service;

import com.example.webflux.application.port.in.DeleteSessionUseCase;
import com.example.webflux.application.port.out.DeleteSessionPort;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Mono;

@RequiredArgsConstructor
@Service
public class DeleteSessionService implements DeleteSessionUseCase {

    private final DeleteSessionPort deleteSessionPort;

    @Override
    public Mono<Void> deleteSession(Command command) {
        return deleteSessionPort.deleteSession(command.getUserId());
    }
}
