package com.example.webflux.application.service;

import com.example.webflux.application.port.in.GetSessionUseCase;
import com.example.webflux.application.port.out.GetSessionPort;
import com.example.webflux.domain.Session;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Mono;

@RequiredArgsConstructor
@Service
public class GetSessionService implements GetSessionUseCase {

    private final GetSessionPort getSessionPort;

    @Override
    public Mono<Session> getSession(Command command) {
        return getSessionPort.getSession(command.getUserId());
    }
}
