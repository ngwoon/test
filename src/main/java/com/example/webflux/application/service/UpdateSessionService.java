package com.example.webflux.application.service;

import com.example.webflux.application.port.in.UpdateSessionUseCase;
import com.example.webflux.application.port.out.UpdateSessionPort;
import com.example.webflux.domain.Session;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Mono;

@RequiredArgsConstructor
@Service
public class UpdateSessionService implements UpdateSessionUseCase {

    private final UpdateSessionPort updateSessionPort;

    @Override
    public Mono<Session> updateSession(Command command) {
        return updateSessionPort.updateSession(command.getUserId());
    }
}
