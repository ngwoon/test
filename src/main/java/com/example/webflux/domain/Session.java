package com.example.webflux.domain;

import java.time.LocalDateTime;

public interface Session {
    String getUsername();
    String getUserEmail();
    Long getUserId();

    LocalDateTime getExpirationTime();
}
